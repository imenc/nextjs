"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/feedback";
exports.ids = ["pages/api/feedback"];
exports.modules = {

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("fs");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("path");

/***/ }),

/***/ "./pages/api/feedback.js?c5be":
/*!*******************************!*\
  !*** ./pages/api/feedback.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"buildFeedbackPath\": () => (/* binding */ buildFeedbackPath),\n/* harmony export */   \"extractFeedback\": () => (/* binding */ extractFeedback),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ \"fs\");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_1__);\n\n\nfunction buildFeedbackPath() {\n    return path__WEBPACK_IMPORTED_MODULE_1___default().join(process.cwd(), 'data', 'feedback.json');\n}\nfunction extractFeedback(filePath) {\n    const fileData = fs__WEBPACK_IMPORTED_MODULE_0___default().readFileSync(filePath);\n    const data = JSON.parse(fileData);\n    return data;\n}\nfunction handler(req, res) {\n    if (req.method === 'POST') {\n        // Extract data\n        const email = req.body.email;\n        const feedbackText = req.body.text;\n        const newFeedback = {\n            id: new Date().toISOString(),\n            email: email,\n            text: feedbackText\n        };\n        // store that in a database or in a file\n        const filePath = buildFeedbackPath();\n        const data = extractFeedback(filePath);\n        data.push(newFeedback);\n        fs__WEBPACK_IMPORTED_MODULE_0___default().writeFileSync(filePath, JSON.stringify(data));\n        res.status(201).json({\n            message: 'Success!',\n            feeback: newFeedback\n        });\n    } else {\n        const filePath = buildFeedbackPath();\n        const data = extractFeedback(filePath);\n        res.status(200).json({\n            feedback: data\n        });\n    }\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9hcGkvZmVlZGJhY2suanM/YzViZS5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQW1CO0FBQ0k7QUFFaEIsU0FBU0UsaUJBQWlCLEdBQUcsQ0FBQztJQUNqQyxNQUFNLENBQUNELGdEQUFTLENBQUNHLE9BQU8sQ0FBQ0MsR0FBRyxJQUFJLENBQU0sT0FBRSxDQUFlO0FBQzNELENBQUM7QUFFTSxTQUFTQyxlQUFlLENBQUNDLFFBQVEsRUFBRSxDQUFDO0lBQ3ZDLEtBQUssQ0FBQ0MsUUFBUSxHQUFHUixzREFBZSxDQUFDTyxRQUFRO0lBQ3pDLEtBQUssQ0FBQ0csSUFBSSxHQUFHQyxJQUFJLENBQUNDLEtBQUssQ0FBQ0osUUFBUTtJQUNoQyxNQUFNLENBQUNFLElBQUk7QUFDZixDQUFDO1NBRVFHLE9BQU8sQ0FBQ0MsR0FBRyxFQUFFQyxHQUFHLEVBQUUsQ0FBQztJQUN4QixFQUFFLEVBQUVELEdBQUcsQ0FBQ0UsTUFBTSxLQUFLLENBQU0sT0FBRSxDQUFDO1FBQ3hCLEVBQWU7UUFDZixLQUFLLENBQUNDLEtBQUssR0FBR0gsR0FBRyxDQUFDSSxJQUFJLENBQUNELEtBQUs7UUFDNUIsS0FBSyxDQUFDRSxZQUFZLEdBQUdMLEdBQUcsQ0FBQ0ksSUFBSSxDQUFDRSxJQUFJO1FBRWxDLEtBQUssQ0FBQ0MsV0FBVyxHQUFHLENBQUM7WUFDakJDLEVBQUUsRUFBRSxHQUFHLENBQUNDLElBQUksR0FBR0MsV0FBVztZQUMxQlAsS0FBSyxFQUFFQSxLQUFLO1lBQ1pHLElBQUksRUFBRUQsWUFBWTtRQUN0QixDQUFDO1FBRUQsRUFBd0M7UUFDeEMsS0FBSyxDQUFDWixRQUFRLEdBQUdMLGlCQUFpQjtRQUNsQyxLQUFLLENBQUNRLElBQUksR0FBR0osZUFBZSxDQUFDQyxRQUFRO1FBQ3JDRyxJQUFJLENBQUNlLElBQUksQ0FBQ0osV0FBVztRQUNyQnJCLHVEQUFnQixDQUFDTyxRQUFRLEVBQUVJLElBQUksQ0FBQ2dCLFNBQVMsQ0FBQ2pCLElBQUk7UUFDOUNLLEdBQUcsQ0FBQ2EsTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7WUFBQ0MsT0FBTyxFQUFFLENBQVU7WUFBRUMsT0FBTyxFQUFFVixXQUFXO1FBQUMsQ0FBQztJQUN0RSxDQUFDLE1BQU0sQ0FBQztRQUNKLEtBQUssQ0FBQ2QsUUFBUSxHQUFHTCxpQkFBaUI7UUFDbEMsS0FBSyxDQUFDUSxJQUFJLEdBQUdKLGVBQWUsQ0FBQ0MsUUFBUTtRQUNyQ1EsR0FBRyxDQUFDYSxNQUFNLENBQUMsR0FBRyxFQUFFQyxJQUFJLENBQUMsQ0FBQztZQUFDRyxRQUFRLEVBQUV0QixJQUFJO1FBQUMsQ0FBQztJQUMzQyxDQUFDO0FBQ0wsQ0FBQztBQUVELGlFQUFlRyxPQUFPLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9yZWFjdC1jb3Vyc2UwNi8uL3BhZ2VzL2FwaS9mZWVkYmFjay5qcz9mN2M4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcblxuZXhwb3J0IGZ1bmN0aW9uIGJ1aWxkRmVlZGJhY2tQYXRoKCkge1xuICAgIHJldHVybiBwYXRoLmpvaW4ocHJvY2Vzcy5jd2QoKSwgJ2RhdGEnLCAnZmVlZGJhY2suanNvbicpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZXh0cmFjdEZlZWRiYWNrKGZpbGVQYXRoKSB7XG4gICAgY29uc3QgZmlsZURhdGEgPSBmcy5yZWFkRmlsZVN5bmMoZmlsZVBhdGgpO1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGZpbGVEYXRhKTtcbiAgICByZXR1cm4gZGF0YTtcbn1cblxuZnVuY3Rpb24gaGFuZGxlcihyZXEsIHJlcykge1xuICAgIGlmIChyZXEubWV0aG9kID09PSAnUE9TVCcpIHtcbiAgICAgICAgLy8gRXh0cmFjdCBkYXRhXG4gICAgICAgIGNvbnN0IGVtYWlsID0gcmVxLmJvZHkuZW1haWw7XG4gICAgICAgIGNvbnN0IGZlZWRiYWNrVGV4dCA9IHJlcS5ib2R5LnRleHQ7XG5cbiAgICAgICAgY29uc3QgbmV3RmVlZGJhY2sgPSB7XG4gICAgICAgICAgICBpZDogbmV3IERhdGUoKS50b0lTT1N0cmluZygpLFxuICAgICAgICAgICAgZW1haWw6IGVtYWlsLFxuICAgICAgICAgICAgdGV4dDogZmVlZGJhY2tUZXh0LFxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHN0b3JlIHRoYXQgaW4gYSBkYXRhYmFzZSBvciBpbiBhIGZpbGVcbiAgICAgICAgY29uc3QgZmlsZVBhdGggPSBidWlsZEZlZWRiYWNrUGF0aCgpO1xuICAgICAgICBjb25zdCBkYXRhID0gZXh0cmFjdEZlZWRiYWNrKGZpbGVQYXRoKTtcbiAgICAgICAgZGF0YS5wdXNoKG5ld0ZlZWRiYWNrKTtcbiAgICAgICAgZnMud3JpdGVGaWxlU3luYyhmaWxlUGF0aCwgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xuICAgICAgICByZXMuc3RhdHVzKDIwMSkuanNvbih7IG1lc3NhZ2U6ICdTdWNjZXNzIScsIGZlZWJhY2s6IG5ld0ZlZWRiYWNrIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IGZpbGVQYXRoID0gYnVpbGRGZWVkYmFja1BhdGgoKTtcbiAgICAgICAgY29uc3QgZGF0YSA9IGV4dHJhY3RGZWVkYmFjayhmaWxlUGF0aCk7XG4gICAgICAgIHJlcy5zdGF0dXMoMjAwKS5qc29uKHsgZmVlZGJhY2s6IGRhdGEgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBoYW5kbGVyO1xuIl0sIm5hbWVzIjpbImZzIiwicGF0aCIsImJ1aWxkRmVlZGJhY2tQYXRoIiwiam9pbiIsInByb2Nlc3MiLCJjd2QiLCJleHRyYWN0RmVlZGJhY2siLCJmaWxlUGF0aCIsImZpbGVEYXRhIiwicmVhZEZpbGVTeW5jIiwiZGF0YSIsIkpTT04iLCJwYXJzZSIsImhhbmRsZXIiLCJyZXEiLCJyZXMiLCJtZXRob2QiLCJlbWFpbCIsImJvZHkiLCJmZWVkYmFja1RleHQiLCJ0ZXh0IiwibmV3RmVlZGJhY2siLCJpZCIsIkRhdGUiLCJ0b0lTT1N0cmluZyIsInB1c2giLCJ3cml0ZUZpbGVTeW5jIiwic3RyaW5naWZ5Iiwic3RhdHVzIiwianNvbiIsIm1lc3NhZ2UiLCJmZWViYWNrIiwiZmVlZGJhY2siXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/api/feedback.js?c5be\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/api/feedback.js?c5be"));
module.exports = __webpack_exports__;

})();