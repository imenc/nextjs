"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/feedback";
exports.ids = ["pages/feedback"];
exports.modules = {

/***/ "./pages/api/feedback.js?c014":
/*!*******************************!*\
  !*** ./pages/api/feedback.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"buildFeedbackPath\": () => (/* binding */ buildFeedbackPath),\n/* harmony export */   \"extractFeedback\": () => (/* binding */ extractFeedback),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ \"fs\");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_1__);\n\n\nfunction buildFeedbackPath() {\n    return path__WEBPACK_IMPORTED_MODULE_1___default().join(process.cwd(), 'data', 'feedback.json');\n}\nfunction extractFeedback(filePath) {\n    const fileData = fs__WEBPACK_IMPORTED_MODULE_0___default().readFileSync(filePath);\n    const data = JSON.parse(fileData);\n    return data;\n}\nfunction handler(req, res) {\n    if (req.method === 'POST') {\n        // Extract data\n        const email = req.body.email;\n        const feedbackText = req.body.text;\n        const newFeedback = {\n            id: new Date().toISOString(),\n            email: email,\n            text: feedbackText\n        };\n        // store that in a database or in a file\n        const filePath = buildFeedbackPath();\n        const data = extractFeedback(filePath);\n        data.push(newFeedback);\n        fs__WEBPACK_IMPORTED_MODULE_0___default().writeFileSync(filePath, JSON.stringify(data));\n        res.status(201).json({\n            message: 'Success!',\n            feeback: newFeedback\n        });\n    } else {\n        const filePath = buildFeedbackPath();\n        const data = extractFeedback(filePath);\n        res.status(200).json({\n            feedback: data\n        });\n    }\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9hcGkvZmVlZGJhY2suanM/YzAxNC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQW1CO0FBQ0k7QUFFaEIsU0FBU0UsaUJBQWlCLEdBQUcsQ0FBQztJQUNqQyxNQUFNLENBQUNELGdEQUFTLENBQUNHLE9BQU8sQ0FBQ0MsR0FBRyxJQUFJLENBQU0sT0FBRSxDQUFlO0FBQzNELENBQUM7QUFFTSxTQUFTQyxlQUFlLENBQUNDLFFBQVEsRUFBRSxDQUFDO0lBQ3ZDLEtBQUssQ0FBQ0MsUUFBUSxHQUFHUixzREFBZSxDQUFDTyxRQUFRO0lBQ3pDLEtBQUssQ0FBQ0csSUFBSSxHQUFHQyxJQUFJLENBQUNDLEtBQUssQ0FBQ0osUUFBUTtJQUNoQyxNQUFNLENBQUNFLElBQUk7QUFDZixDQUFDO1NBRVFHLE9BQU8sQ0FBQ0MsR0FBRyxFQUFFQyxHQUFHLEVBQUUsQ0FBQztJQUN4QixFQUFFLEVBQUVELEdBQUcsQ0FBQ0UsTUFBTSxLQUFLLENBQU0sT0FBRSxDQUFDO1FBQ3hCLEVBQWU7UUFDZixLQUFLLENBQUNDLEtBQUssR0FBR0gsR0FBRyxDQUFDSSxJQUFJLENBQUNELEtBQUs7UUFDNUIsS0FBSyxDQUFDRSxZQUFZLEdBQUdMLEdBQUcsQ0FBQ0ksSUFBSSxDQUFDRSxJQUFJO1FBRWxDLEtBQUssQ0FBQ0MsV0FBVyxHQUFHLENBQUM7WUFDakJDLEVBQUUsRUFBRSxHQUFHLENBQUNDLElBQUksR0FBR0MsV0FBVztZQUMxQlAsS0FBSyxFQUFFQSxLQUFLO1lBQ1pHLElBQUksRUFBRUQsWUFBWTtRQUN0QixDQUFDO1FBRUQsRUFBd0M7UUFDeEMsS0FBSyxDQUFDWixRQUFRLEdBQUdMLGlCQUFpQjtRQUNsQyxLQUFLLENBQUNRLElBQUksR0FBR0osZUFBZSxDQUFDQyxRQUFRO1FBQ3JDRyxJQUFJLENBQUNlLElBQUksQ0FBQ0osV0FBVztRQUNyQnJCLHVEQUFnQixDQUFDTyxRQUFRLEVBQUVJLElBQUksQ0FBQ2dCLFNBQVMsQ0FBQ2pCLElBQUk7UUFDOUNLLEdBQUcsQ0FBQ2EsTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7WUFBQ0MsT0FBTyxFQUFFLENBQVU7WUFBRUMsT0FBTyxFQUFFVixXQUFXO1FBQUMsQ0FBQztJQUN0RSxDQUFDLE1BQU0sQ0FBQztRQUNKLEtBQUssQ0FBQ2QsUUFBUSxHQUFHTCxpQkFBaUI7UUFDbEMsS0FBSyxDQUFDUSxJQUFJLEdBQUdKLGVBQWUsQ0FBQ0MsUUFBUTtRQUNyQ1EsR0FBRyxDQUFDYSxNQUFNLENBQUMsR0FBRyxFQUFFQyxJQUFJLENBQUMsQ0FBQztZQUFDRyxRQUFRLEVBQUV0QixJQUFJO1FBQUMsQ0FBQztJQUMzQyxDQUFDO0FBQ0wsQ0FBQztBQUVELGlFQUFlRyxPQUFPLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9yZWFjdC1jb3Vyc2UwNi8uL3BhZ2VzL2FwaS9mZWVkYmFjay5qcz9mN2M4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcblxuZXhwb3J0IGZ1bmN0aW9uIGJ1aWxkRmVlZGJhY2tQYXRoKCkge1xuICAgIHJldHVybiBwYXRoLmpvaW4ocHJvY2Vzcy5jd2QoKSwgJ2RhdGEnLCAnZmVlZGJhY2suanNvbicpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZXh0cmFjdEZlZWRiYWNrKGZpbGVQYXRoKSB7XG4gICAgY29uc3QgZmlsZURhdGEgPSBmcy5yZWFkRmlsZVN5bmMoZmlsZVBhdGgpO1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGZpbGVEYXRhKTtcbiAgICByZXR1cm4gZGF0YTtcbn1cblxuZnVuY3Rpb24gaGFuZGxlcihyZXEsIHJlcykge1xuICAgIGlmIChyZXEubWV0aG9kID09PSAnUE9TVCcpIHtcbiAgICAgICAgLy8gRXh0cmFjdCBkYXRhXG4gICAgICAgIGNvbnN0IGVtYWlsID0gcmVxLmJvZHkuZW1haWw7XG4gICAgICAgIGNvbnN0IGZlZWRiYWNrVGV4dCA9IHJlcS5ib2R5LnRleHQ7XG5cbiAgICAgICAgY29uc3QgbmV3RmVlZGJhY2sgPSB7XG4gICAgICAgICAgICBpZDogbmV3IERhdGUoKS50b0lTT1N0cmluZygpLFxuICAgICAgICAgICAgZW1haWw6IGVtYWlsLFxuICAgICAgICAgICAgdGV4dDogZmVlZGJhY2tUZXh0LFxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHN0b3JlIHRoYXQgaW4gYSBkYXRhYmFzZSBvciBpbiBhIGZpbGVcbiAgICAgICAgY29uc3QgZmlsZVBhdGggPSBidWlsZEZlZWRiYWNrUGF0aCgpO1xuICAgICAgICBjb25zdCBkYXRhID0gZXh0cmFjdEZlZWRiYWNrKGZpbGVQYXRoKTtcbiAgICAgICAgZGF0YS5wdXNoKG5ld0ZlZWRiYWNrKTtcbiAgICAgICAgZnMud3JpdGVGaWxlU3luYyhmaWxlUGF0aCwgSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xuICAgICAgICByZXMuc3RhdHVzKDIwMSkuanNvbih7IG1lc3NhZ2U6ICdTdWNjZXNzIScsIGZlZWJhY2s6IG5ld0ZlZWRiYWNrIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IGZpbGVQYXRoID0gYnVpbGRGZWVkYmFja1BhdGgoKTtcbiAgICAgICAgY29uc3QgZGF0YSA9IGV4dHJhY3RGZWVkYmFjayhmaWxlUGF0aCk7XG4gICAgICAgIHJlcy5zdGF0dXMoMjAwKS5qc29uKHsgZmVlZGJhY2s6IGRhdGEgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBoYW5kbGVyO1xuIl0sIm5hbWVzIjpbImZzIiwicGF0aCIsImJ1aWxkRmVlZGJhY2tQYXRoIiwiam9pbiIsInByb2Nlc3MiLCJjd2QiLCJleHRyYWN0RmVlZGJhY2siLCJmaWxlUGF0aCIsImZpbGVEYXRhIiwicmVhZEZpbGVTeW5jIiwiZGF0YSIsIkpTT04iLCJwYXJzZSIsImhhbmRsZXIiLCJyZXEiLCJyZXMiLCJtZXRob2QiLCJlbWFpbCIsImJvZHkiLCJmZWVkYmFja1RleHQiLCJ0ZXh0IiwibmV3RmVlZGJhY2siLCJpZCIsIkRhdGUiLCJ0b0lTT1N0cmluZyIsInB1c2giLCJ3cml0ZUZpbGVTeW5jIiwic3RyaW5naWZ5Iiwic3RhdHVzIiwianNvbiIsIm1lc3NhZ2UiLCJmZWViYWNrIiwiZmVlZGJhY2siXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/api/feedback.js?c014\n");

/***/ }),

/***/ "./pages/feedback/index.js":
/*!*********************************!*\
  !*** ./pages/feedback/index.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"getStaticProps\": () => (/* binding */ getStaticProps),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _api_feedback__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/feedback */ \"./pages/api/feedback.js?c014\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nfunction FeedbackPage(props) {\n    const { 0: feedbackData , 1: setFeedbackData  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)();\n    function loadFeedbackHandler(id) {\n        fetch(`/api/${id}`).then((response)=>response.json()\n        ).then((data)=>{\n            setFeedbackData(data.feedback);\n        }); // /api/some-feedback-id\n    }\n    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_2__.Fragment, {\n        __source: {\n            fileName: \"/Users/bsi/udemy/nextjs/react-course06/pages/feedback/index.js\",\n            lineNumber: 16,\n            columnNumber: 9\n        },\n        __self: this,\n        children: [\n            feedbackData && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"p\", {\n                __source: {\n                    fileName: \"/Users/bsi/udemy/nextjs/react-course06/pages/feedback/index.js\",\n                    lineNumber: 17,\n                    columnNumber: 30\n                },\n                __self: this,\n                children: feedbackData.email\n            }),\n            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"ul\", {\n                __source: {\n                    fileName: \"/Users/bsi/udemy/nextjs/react-course06/pages/feedback/index.js\",\n                    lineNumber: 18,\n                    columnNumber: 13\n                },\n                __self: this,\n                children: props.feedbackItems.map((item)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(\"li\", {\n                        __source: {\n                            fileName: \"/Users/bsi/udemy/nextjs/react-course06/pages/feedback/index.js\",\n                            lineNumber: 20,\n                            columnNumber: 17\n                        },\n                        __self: this,\n                        children: [\n                            item.text,\n                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"button\", {\n                                onClick: loadFeedbackHandler.bind(null, item.id),\n                                __source: {\n                                    fileName: \"/Users/bsi/udemy/nextjs/react-course06/pages/feedback/index.js\",\n                                    lineNumber: 22,\n                                    columnNumber: 21\n                                },\n                                __self: this,\n                                children: \"Show Details\"\n                            })\n                        ]\n                    }, item.id)\n                )\n            })\n        ]\n    }));\n}\nasync function getStaticProps() {\n    const filePath = (0,_api_feedback__WEBPACK_IMPORTED_MODULE_1__.buildFeedbackPath)();\n    const data = (0,_api_feedback__WEBPACK_IMPORTED_MODULE_1__.extractFeedback)(filePath);\n    return {\n        props: {\n            feedbackItems: data\n        }\n    };\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FeedbackPage);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9mZWVkYmFjay9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFvRTtBQUMxQjtTQUVqQ0ksWUFBWSxDQUFDQyxLQUFLLEVBQUUsQ0FBQztJQUMxQixLQUFLLE1BQUVDLFlBQVksTUFBRUMsZUFBZSxNQUFJTCwrQ0FBUTthQUV2Q00sbUJBQW1CLENBQUNDLEVBQUUsRUFBRSxDQUFDO1FBQzlCQyxLQUFLLEVBQUUsS0FBSyxFQUFFRCxFQUFFLElBQ1hFLElBQUksRUFBQ0MsUUFBUSxHQUFJQSxRQUFRLENBQUNDLElBQUk7VUFDOUJGLElBQUksRUFBRUcsSUFBSSxHQUFJLENBQUM7WUFDWlAsZUFBZSxDQUFDTyxJQUFJLENBQUNDLFFBQVE7UUFDakMsQ0FBQyxFQUFNLENBQXdCO0lBQ3ZDLENBQUM7SUFFRCxNQUFNLHVFQUNEWiwyQ0FBUTs7Ozs7Ozs7WUFDSkcsWUFBWSx5RUFBS1UsQ0FBQzs7Ozs7OzswQkFBRVYsWUFBWSxDQUFDVyxLQUFLOztpRkFDdENDLENBQUU7Ozs7Ozs7MEJBQ0ZiLEtBQUssQ0FBQ2MsYUFBYSxDQUFDQyxHQUFHLEVBQUdDLElBQUkseUVBQzFCQyxDQUFFOzs7Ozs7Ozs0QkFDRUQsSUFBSSxDQUFDRSxJQUFJO2lHQUNUQyxDQUFNO2dDQUFDQyxPQUFPLEVBQUVqQixtQkFBbUIsQ0FBQ2tCLElBQUksQ0FBQyxJQUFJLEVBQUVMLElBQUksQ0FBQ1osRUFBRTs7Ozs7OzswQ0FBRyxDQUUxRDs7O3VCQUpLWSxJQUFJLENBQUNaLEVBQUU7Ozs7O0FBV2hDLENBQUM7QUFFTSxlQUFla0IsY0FBYyxHQUFHLENBQUM7SUFDcEMsS0FBSyxDQUFDQyxRQUFRLEdBQUc1QixnRUFBaUI7SUFDbEMsS0FBSyxDQUFDYyxJQUFJLEdBQUdiLDhEQUFlLENBQUMyQixRQUFRO0lBQ3JDLE1BQU0sQ0FBQyxDQUFDO1FBQ0p2QixLQUFLLEVBQUUsQ0FBQztZQUNKYyxhQUFhLEVBQUVMLElBQUk7UUFDdkIsQ0FBQztJQUNMLENBQUM7QUFDTCxDQUFDO0FBRUQsaUVBQWVWLFlBQVksRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL3JlYWN0LWNvdXJzZTA2Ly4vcGFnZXMvZmVlZGJhY2svaW5kZXguanM/MjRiYiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBidWlsZEZlZWRiYWNrUGF0aCwgZXh0cmFjdEZlZWRiYWNrIH0gZnJvbSAnLi4vYXBpL2ZlZWRiYWNrJztcbmltcG9ydCB7IHVzZVN0YXRlLCBGcmFnbWVudCB9IGZyb20gJ3JlYWN0JztcblxuZnVuY3Rpb24gRmVlZGJhY2tQYWdlKHByb3BzKSB7XG4gICAgY29uc3QgW2ZlZWRiYWNrRGF0YSwgc2V0RmVlZGJhY2tEYXRhXSA9IHVzZVN0YXRlKCk7XG5cbiAgICBmdW5jdGlvbiBsb2FkRmVlZGJhY2tIYW5kbGVyKGlkKSB7XG4gICAgICAgIGZldGNoKGAvYXBpLyR7aWR9YClcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHJlc3BvbnNlLmpzb24oKSlcbiAgICAgICAgICAgIC50aGVuKCBkYXRhID0+IHtcbiAgICAgICAgICAgICAgICBzZXRGZWVkYmFja0RhdGEoZGF0YS5mZWVkYmFjayk7XG4gICAgICAgICAgICB9KTsgICAgLy8gL2FwaS9zb21lLWZlZWRiYWNrLWlkXG4gICAgfVxuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPEZyYWdtZW50PlxuICAgICAgICAgICAge2ZlZWRiYWNrRGF0YSAmJiA8cD57ZmVlZGJhY2tEYXRhLmVtYWlsfTwvcD59XG4gICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICB7cHJvcHMuZmVlZGJhY2tJdGVtcy5tYXAoIChpdGVtKSA9PiAoXG4gICAgICAgICAgICAgICAgPGxpIGtleT17aXRlbS5pZH0+XG4gICAgICAgICAgICAgICAgICAgIHtpdGVtLnRleHR9XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17bG9hZEZlZWRiYWNrSGFuZGxlci5iaW5kKG51bGwsIGl0ZW0uaWQpfT5cbiAgICAgICAgICAgICAgICAgICAgICAgIFNob3cgRGV0YWlsc1xuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L2xpPiBcbiAgICAgICAgICAgICkpfVxuICAgICAgICA8L3VsPlxuICAgICAgICA8L0ZyYWdtZW50PlxuICAgICAgICBcbiAgICApO1xufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0U3RhdGljUHJvcHMoKSB7XG4gICAgY29uc3QgZmlsZVBhdGggPSBidWlsZEZlZWRiYWNrUGF0aCgpO1xuICAgIGNvbnN0IGRhdGEgPSBleHRyYWN0RmVlZGJhY2soZmlsZVBhdGgpO1xuICAgIHJldHVybiB7XG4gICAgICAgIHByb3BzOiB7XG4gICAgICAgICAgICBmZWVkYmFja0l0ZW1zOiBkYXRhXG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEZlZWRiYWNrUGFnZTtcbiJdLCJuYW1lcyI6WyJidWlsZEZlZWRiYWNrUGF0aCIsImV4dHJhY3RGZWVkYmFjayIsInVzZVN0YXRlIiwiRnJhZ21lbnQiLCJGZWVkYmFja1BhZ2UiLCJwcm9wcyIsImZlZWRiYWNrRGF0YSIsInNldEZlZWRiYWNrRGF0YSIsImxvYWRGZWVkYmFja0hhbmRsZXIiLCJpZCIsImZldGNoIiwidGhlbiIsInJlc3BvbnNlIiwianNvbiIsImRhdGEiLCJmZWVkYmFjayIsInAiLCJlbWFpbCIsInVsIiwiZmVlZGJhY2tJdGVtcyIsIm1hcCIsIml0ZW0iLCJsaSIsInRleHQiLCJidXR0b24iLCJvbkNsaWNrIiwiYmluZCIsImdldFN0YXRpY1Byb3BzIiwiZmlsZVBhdGgiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/feedback/index.js\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-runtime":
/*!************************************!*\
  !*** external "react/jsx-runtime" ***!
  \************************************/
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("fs");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("path");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/feedback/index.js"));
module.exports = __webpack_exports__;

})();