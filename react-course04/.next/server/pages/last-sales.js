"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/last-sales";
exports.ids = ["pages/last-sales"];
exports.modules = {

/***/ "./pages/last-sales.js":
/*!*****************************!*\
  !*** ./pages/last-sales.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"getStaticProps\": () => (/* binding */ getStaticProps),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! swr */ \"swr\");\n/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nfunction LastSalesPage(props) {\n    const { 0: sales , 1: setSales  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(props.sales);\n    const { data , error  } = swr__WEBPACK_IMPORTED_MODULE_2___default()('https://react-course04-default-rtdb.asia-southeast1.firebasedatabase.app/sales.json', (url)=>fetch(url).then((res)=>res.json()\n        )\n    );\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{\n        if (data) {\n            const transformedSales = [];\n            for(const key in data){\n                transformedSales.push({\n                    id: key,\n                    username: data[key].username,\n                    volume: data[key].volume\n                });\n            }\n            setSales(transformedSales);\n        }\n    }, [\n        data\n    ]);\n    if (error) {\n        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"p\", {\n            __source: {\n                fileName: \"/Users/bsi_2019_05/udemy/nextjs/react-course04/pages/last-sales.js\",\n                lineNumber: 26,\n                columnNumber: 16\n            },\n            __self: this,\n            children: \"Failed to load.\"\n        }));\n    }\n    if (!data && !sales) {\n        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"p\", {\n            __source: {\n                fileName: \"/Users/bsi_2019_05/udemy/nextjs/react-course04/pages/last-sales.js\",\n                lineNumber: 30,\n                columnNumber: 16\n            },\n            __self: this,\n            children: \"Loading...\"\n        }));\n    }\n    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"ul\", {\n        __source: {\n            fileName: \"/Users/bsi_2019_05/udemy/nextjs/react-course04/pages/last-sales.js\",\n            lineNumber: 33,\n            columnNumber: 9\n        },\n        __self: this,\n        children: sales.map((sale)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(\"li\", {\n                __source: {\n                    fileName: \"/Users/bsi_2019_05/udemy/nextjs/react-course04/pages/last-sales.js\",\n                    lineNumber: 35,\n                    columnNumber: 13\n                },\n                __self: this,\n                children: [\n                    sale.username,\n                    \" - Rp \",\n                    sale.volume\n                ]\n            }, sale.id)\n        )\n    }));\n}\nasync function getStaticProps() {\n    const response = await fetch('https://react-course04-default-rtdb.asia-southeast1.firebasedatabase.app/sales.json');\n    const data = await response.json();\n    const transformedSales = [];\n    for(const key in data){\n        transformedSales.push({\n            id: key,\n            username: data[key].username,\n            volume: data[key].volume\n        });\n    }\n    return {\n        props: {\n            sales: transformedSales\n        }\n    };\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LastSalesPage);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9sYXN0LXNhbGVzLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUEyQztBQUNuQjtTQUVmRyxhQUFhLENBQUNDLEtBQUssRUFBRSxDQUFDO0lBQzNCLEtBQUssTUFBRUMsS0FBSyxNQUFFQyxRQUFRLE1BQUlMLCtDQUFRLENBQUNHLEtBQUssQ0FBQ0MsS0FBSztJQUM5QyxLQUFLLENBQUMsQ0FBQyxDQUFDRSxJQUFJLEdBQUVDLEtBQUssRUFBQyxDQUFDLEdBQUdOLDBDQUFNLENBQzFCLENBQXFGLHVGQUNwRk8sR0FBRyxHQUFLQyxLQUFLLENBQUNELEdBQUcsRUFBRUUsSUFBSSxFQUFDQyxHQUFHLEdBQUlBLEdBQUcsQ0FBQ0MsSUFBSTs7O0lBRzVDYixnREFBUyxLQUFPLENBQUM7UUFDYixFQUFFLEVBQUVPLElBQUksRUFBRSxDQUFDO1lBQ1gsS0FBSyxDQUFDTyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7WUFDdkIsR0FBRyxDQUFFLEtBQUssQ0FBQ0MsR0FBRyxJQUFJUixJQUFJLENBQUUsQ0FBQztnQkFDckJPLGdCQUFnQixDQUFDRSxJQUFJLENBQUMsQ0FBQztvQkFDbkJDLEVBQUUsRUFBRUYsR0FBRztvQkFDUEcsUUFBUSxFQUFFWCxJQUFJLENBQUNRLEdBQUcsRUFBRUcsUUFBUTtvQkFDNUJDLE1BQU0sRUFBRVosSUFBSSxDQUFDUSxHQUFHLEVBQUVJLE1BQU07Z0JBQzVCLENBQUM7WUFDTCxDQUFDO1lBQ0RiLFFBQVEsQ0FBQ1EsZ0JBQWdCO1FBQzdCLENBQUM7SUFDTCxDQUFDLEVBQUUsQ0FBQ1A7UUFBQUEsSUFBSTtJQUFBLENBQUM7SUFFVCxFQUFFLEVBQUVDLEtBQUssRUFBRSxDQUFDO1FBQ1IsTUFBTSxzRUFBRVksQ0FBQzs7Ozs7OztzQkFBQyxDQUFlOztJQUM3QixDQUFDO0lBRUQsRUFBRSxHQUFHYixJQUFJLEtBQUtGLEtBQUssRUFBRSxDQUFDO1FBQ2xCLE1BQU0sc0VBQUVlLENBQUM7Ozs7Ozs7c0JBQUMsQ0FBVTs7SUFDeEIsQ0FBQztJQUNELE1BQU0sc0VBQ0RDLENBQUU7Ozs7Ozs7a0JBQ0ZoQixLQUFLLENBQUNpQixHQUFHLEVBQUVDLElBQUkseUVBQ1hDLENBQUU7Ozs7Ozs7O29CQUNGRCxJQUFJLENBQUNMLFFBQVE7b0JBQUMsQ0FBTTtvQkFBQ0ssSUFBSSxDQUFDSixNQUFNOztlQUR4QkksSUFBSSxDQUFDTixFQUFFOzs7QUFNNUIsQ0FBQztBQUVNLGVBQWVRLGNBQWMsR0FBRyxDQUFDO0lBQ3BDLEtBQUssQ0FBQ0MsUUFBUSxHQUFHLEtBQUssQ0FBQ2hCLEtBQUssQ0FDeEIsQ0FBcUY7SUFFekYsS0FBSyxDQUFDSCxJQUFJLEdBQUcsS0FBSyxDQUFDbUIsUUFBUSxDQUFDYixJQUFJO0lBQ2hDLEtBQUssQ0FBQ0MsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLEdBQUcsQ0FBRSxLQUFLLENBQUNDLEdBQUcsSUFBSVIsSUFBSSxDQUFFLENBQUM7UUFDckJPLGdCQUFnQixDQUFDRSxJQUFJLENBQUMsQ0FBQztZQUNuQkMsRUFBRSxFQUFFRixHQUFHO1lBQ1BHLFFBQVEsRUFBRVgsSUFBSSxDQUFDUSxHQUFHLEVBQUVHLFFBQVE7WUFDNUJDLE1BQU0sRUFBRVosSUFBSSxDQUFDUSxHQUFHLEVBQUVJLE1BQU07UUFDNUIsQ0FBQztJQUNMLENBQUM7SUFDSCxNQUFNLENBQUMsQ0FBQztRQUFDZixLQUFLLEVBQUUsQ0FBQztZQUFDQyxLQUFLLEVBQUVTLGdCQUFnQjtRQUFDLENBQUM7SUFBQyxDQUFDO0FBQy9DLENBQUM7QUFFRCxpRUFBZVgsYUFBYSxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcmVhY3QtY291cnNlMDQvLi9wYWdlcy9sYXN0LXNhbGVzLmpzPzI5MTkiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB1c2VTV1IgZnJvbSAnc3dyJztcblxuZnVuY3Rpb24gTGFzdFNhbGVzUGFnZShwcm9wcykge1xuICAgIGNvbnN0IFtzYWxlcywgc2V0U2FsZXNdID0gdXNlU3RhdGUocHJvcHMuc2FsZXMpO1xuICAgIGNvbnN0IHsgZGF0YSwgZXJyb3IgfSA9IHVzZVNXUihcbiAgICAgICAgJ2h0dHBzOi8vcmVhY3QtY291cnNlMDQtZGVmYXVsdC1ydGRiLmFzaWEtc291dGhlYXN0MS5maXJlYmFzZWRhdGFiYXNlLmFwcC9zYWxlcy5qc29uJyxcbiAgICAgICAgKHVybCkgPT4gZmV0Y2godXJsKS50aGVuKHJlcyA9PiByZXMuanNvbigpKVxuICAgICk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICBjb25zdCB0cmFuc2Zvcm1lZFNhbGVzID0gW107XG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtZWRTYWxlcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6IGtleSxcbiAgICAgICAgICAgICAgICAgICAgdXNlcm5hbWU6IGRhdGFba2V5XS51c2VybmFtZSxcbiAgICAgICAgICAgICAgICAgICAgdm9sdW1lOiBkYXRhW2tleV0udm9sdW1lLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2V0U2FsZXModHJhbnNmb3JtZWRTYWxlcyk7XG4gICAgICAgIH1cbiAgICB9LCBbZGF0YV0pO1xuXG4gICAgaWYgKGVycm9yKSB7XG4gICAgICAgIHJldHVybiA8cD5GYWlsZWQgdG8gbG9hZC48L3A+O1xuICAgIH1cbiAgICBcbiAgICBpZiAoIWRhdGEgJiYgIXNhbGVzKSB7XG4gICAgICAgIHJldHVybiA8cD5Mb2FkaW5nLi4uPC9wPjtcbiAgICB9XG4gICAgcmV0dXJuIChcbiAgICAgICAgPHVsPlxuICAgICAgICB7c2FsZXMubWFwKChzYWxlKSA9PiAoXG4gICAgICAgICAgICA8bGkga2V5PXtzYWxlLmlkfT5cbiAgICAgICAgICAgIHtzYWxlLnVzZXJuYW1lfSAtIFJwIHtzYWxlLnZvbHVtZX1cbiAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICkpfVxuICAgICAgICA8L3VsPlxuICAgICk7XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRTdGF0aWNQcm9wcygpIHtcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGZldGNoKFxuICAgICAgICAnaHR0cHM6Ly9yZWFjdC1jb3Vyc2UwNC1kZWZhdWx0LXJ0ZGIuYXNpYS1zb3V0aGVhc3QxLmZpcmViYXNlZGF0YWJhc2UuYXBwL3NhbGVzLmpzb24nXG4gICAgKTtcbiAgICBjb25zdCBkYXRhID0gYXdhaXQgcmVzcG9uc2UuanNvbigpO1xuICAgIGNvbnN0IHRyYW5zZm9ybWVkU2FsZXMgPSBbXTtcbiAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgIHRyYW5zZm9ybWVkU2FsZXMucHVzaCh7XG4gICAgICAgICAgICBpZDoga2V5LFxuICAgICAgICAgICAgdXNlcm5hbWU6IGRhdGFba2V5XS51c2VybmFtZSxcbiAgICAgICAgICAgIHZvbHVtZTogZGF0YVtrZXldLnZvbHVtZSxcbiAgICAgICAgfSk7XG4gICAgfVxuICByZXR1cm4geyBwcm9wczogeyBzYWxlczogdHJhbnNmb3JtZWRTYWxlcyB9IH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IExhc3RTYWxlc1BhZ2U7Il0sIm5hbWVzIjpbInVzZUVmZmVjdCIsInVzZVN0YXRlIiwidXNlU1dSIiwiTGFzdFNhbGVzUGFnZSIsInByb3BzIiwic2FsZXMiLCJzZXRTYWxlcyIsImRhdGEiLCJlcnJvciIsInVybCIsImZldGNoIiwidGhlbiIsInJlcyIsImpzb24iLCJ0cmFuc2Zvcm1lZFNhbGVzIiwia2V5IiwicHVzaCIsImlkIiwidXNlcm5hbWUiLCJ2b2x1bWUiLCJwIiwidWwiLCJtYXAiLCJzYWxlIiwibGkiLCJnZXRTdGF0aWNQcm9wcyIsInJlc3BvbnNlIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/last-sales.js\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-runtime":
/*!************************************!*\
  !*** external "react/jsx-runtime" ***!
  \************************************/
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ "swr":
/*!**********************!*\
  !*** external "swr" ***!
  \**********************/
/***/ ((module) => {

module.exports = require("swr");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/last-sales.js"));
module.exports = __webpack_exports__;

})();