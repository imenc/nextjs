import fs from 'fs/promises';
// import { redirect } from 'next/dist/server/api-utils';
import path from 'path';
import Link from 'next/link';

// then this
function HomePage(props) {
    const { products } = props;
    return(
        <ul>
            {products.map((product) => (
                <li key={product.id}>
                    <Link href={`/products/${product.id}`}>{product.title}</Link> 
                </li>
            ))}
        </ul>
    );
}

// exec this first - fetches data
export async function getStaticProps() {
    console.log('(Re-)Generating...');
    const filePath = path.join(process.cwd(), 'data', 'dummy-backend.json');
    const jsonData = await fs.readFile(filePath);
    const data = JSON.parse(jsonData);

    if (!data) {
        return {
            redirect: {
                destination: '/no-data'
            }
        }
    }

    if (data.products.length === 0){
        return { notFound: true }
    }

    return{
        props: {
            products: data.products
        },
        revalidate: 120,    // matters in production server
                            // no matters in development server
    };
}

export default HomePage;