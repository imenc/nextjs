import { MongoClient } from 'mongodb';

export async function connectToDatabase() {
  const client = await MongoClient.connect(
    'mongodb+srv://developer:rahasia@cluster0.mv0r5.mongodb.net/auth-demo?retryWrites=true&w=majority'
  );

  return client;
}