/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./components/starting-page/starting-page.module.css":
/*!***********************************************************!*\
  !*** ./components/starting-page/starting-page.module.css ***!
  \***********************************************************/
/***/ ((module) => {

eval("// Exports\nmodule.exports = {\n\t\"starting\": \"starting-page_starting__eF8kV\"\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb21wb25lbnRzL3N0YXJ0aW5nLXBhZ2Uvc3RhcnRpbmctcGFnZS5tb2R1bGUuY3NzLmpzIiwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcmVhY3QtY291cnNlMTEvLi9jb21wb25lbnRzL3N0YXJ0aW5nLXBhZ2Uvc3RhcnRpbmctcGFnZS5tb2R1bGUuY3NzPzQ4MjUiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gRXhwb3J0c1xubW9kdWxlLmV4cG9ydHMgPSB7XG5cdFwic3RhcnRpbmdcIjogXCJzdGFydGluZy1wYWdlX3N0YXJ0aW5nX19lRjhrVlwiXG59O1xuIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./components/starting-page/starting-page.module.css\n");

/***/ }),

/***/ "./components/starting-page/starting-page.js":
/*!***************************************************!*\
  !*** ./components/starting-page/starting-page.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _starting_page_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./starting-page.module.css */ \"./components/starting-page/starting-page.module.css\");\n/* harmony import */ var _starting_page_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_starting_page_module_css__WEBPACK_IMPORTED_MODULE_1__);\n\n\nfunction StartingPageContent() {\n    // Show Link to Login page if NOT auth\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"section\", {\n        className: (_starting_page_module_css__WEBPACK_IMPORTED_MODULE_1___default().starting),\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n            children: \"Welcome on Board!\"\n        }, void 0, false, {\n            fileName: \"/Users/bsi/udemy/nextjs/react-course11/components/starting-page/starting-page.js\",\n            lineNumber: 8,\n            columnNumber: 7\n        }, this)\n    }, void 0, false, {\n        fileName: \"/Users/bsi/udemy/nextjs/react-course11/components/starting-page/starting-page.js\",\n        lineNumber: 7,\n        columnNumber: 5\n    }, this));\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StartingPageContent);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb21wb25lbnRzL3N0YXJ0aW5nLXBhZ2Uvc3RhcnRpbmctcGFnZS5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBZ0Q7U0FFdkNDLG1CQUFtQixHQUFHLENBQUM7SUFDOUIsRUFBc0M7SUFFdEMsTUFBTSw2RUFDSEMsQ0FBTztRQUFDQyxTQUFTLEVBQUVILDJFQUFnQjs4RkFDakNLLENBQUU7c0JBQUMsQ0FBaUI7Ozs7Ozs7Ozs7O0FBRzNCLENBQUM7QUFFRCxpRUFBZUosbUJBQW1CLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9yZWFjdC1jb3Vyc2UxMS8uL2NvbXBvbmVudHMvc3RhcnRpbmctcGFnZS9zdGFydGluZy1wYWdlLmpzPzE3MDYiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGNsYXNzZXMgZnJvbSAnLi9zdGFydGluZy1wYWdlLm1vZHVsZS5jc3MnO1xuXG5mdW5jdGlvbiBTdGFydGluZ1BhZ2VDb250ZW50KCkge1xuICAvLyBTaG93IExpbmsgdG8gTG9naW4gcGFnZSBpZiBOT1QgYXV0aFxuXG4gIHJldHVybiAoXG4gICAgPHNlY3Rpb24gY2xhc3NOYW1lPXtjbGFzc2VzLnN0YXJ0aW5nfT5cbiAgICAgIDxoMT5XZWxjb21lIG9uIEJvYXJkITwvaDE+XG4gICAgPC9zZWN0aW9uPlxuICApO1xufVxuXG5leHBvcnQgZGVmYXVsdCBTdGFydGluZ1BhZ2VDb250ZW50O1xuIl0sIm5hbWVzIjpbImNsYXNzZXMiLCJTdGFydGluZ1BhZ2VDb250ZW50Iiwic2VjdGlvbiIsImNsYXNzTmFtZSIsInN0YXJ0aW5nIiwiaDEiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./components/starting-page/starting-page.js\n");

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _components_starting_page_starting_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/starting-page/starting-page */ \"./components/starting-page/starting-page.js\");\n\n\nfunction HomePage() {\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_starting_page_starting_page__WEBPACK_IMPORTED_MODULE_1__[\"default\"], {}, void 0, false, {\n        fileName: \"/Users/bsi/udemy/nextjs/react-course11/pages/index.js\",\n        lineNumber: 4,\n        columnNumber: 10\n    }, this));\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HomePage);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUEyRTtTQUVsRUMsUUFBUSxHQUFHLENBQUM7SUFDbkIsTUFBTSw2RUFBRUQsK0VBQW1COzs7OztBQUM3QixDQUFDO0FBRUQsaUVBQWVDLFFBQVEsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL3JlYWN0LWNvdXJzZTExLy4vcGFnZXMvaW5kZXguanM/YmVlNyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU3RhcnRpbmdQYWdlQ29udGVudCBmcm9tICcuLi9jb21wb25lbnRzL3N0YXJ0aW5nLXBhZ2Uvc3RhcnRpbmctcGFnZSc7XG5cbmZ1bmN0aW9uIEhvbWVQYWdlKCkge1xuICByZXR1cm4gPFN0YXJ0aW5nUGFnZUNvbnRlbnQgLz47XG59XG5cbmV4cG9ydCBkZWZhdWx0IEhvbWVQYWdlO1xuIl0sIm5hbWVzIjpbIlN0YXJ0aW5nUGFnZUNvbnRlbnQiLCJIb21lUGFnZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/index.js\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.js"));
module.exports = __webpack_exports__;

})();