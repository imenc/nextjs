"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/newsletter";
exports.ids = ["pages/api/newsletter"];
exports.modules = {

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("mongodb");

/***/ }),

/***/ "./pages/api/newsletter.js":
/*!*********************************!*\
  !*** ./pages/api/newsletter.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongodb */ \"mongodb\");\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_0__);\n\nasync function connectDatabase() {\n    const client = await mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect('mongodb+srv://developer:rahasia@cluster0.mv0r5.mongodb.net/events?retryWrites=true&w=majority');\n    return client;\n}\nasync function insertDocument(client, document) {\n    const db = client.db();\n    await db.collection('newsletter').insertOne({\n        document\n    });\n}\nasync function handler(req, res) {\n    if (req.method === 'POST') {\n        const userEmail = req.body.email;\n        if (!userEmail || !userEmail.includes('@')) {\n            res.status(422).json({\n                message: 'Invalid email address.'\n            });\n            return;\n        }\n        let client;\n        try {\n            client = await connectDatabase();\n        } catch (error) {\n            res.status(500).json({\n                message: 'Connecting to the database failed!'\n            });\n            return;\n        }\n        try {\n            await insertDocument(client, {\n                email: userEmail\n            });\n            client.close();\n        } catch (error1) {\n            res.status(500).json({\n                message: 'Inserting data failed!'\n            });\n            return;\n        }\n        res.status(201).json({\n            message: 'Signed up!'\n        });\n    }\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9hcGkvbmV3c2xldHRlci5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7QUFBcUM7ZUFFdEJDLGVBQWUsR0FBRyxDQUFDO0lBQzlCLEtBQUssQ0FBQ0MsTUFBTSxHQUFHLEtBQUssQ0FBQ0Ysd0RBQW1CLENBQ3BDLENBQStGO0lBRW5HLE1BQU0sQ0FBQ0UsTUFBTTtBQUNqQixDQUFDO2VBRWNFLGNBQWMsQ0FBQ0YsTUFBTSxFQUFFRyxRQUFRLEVBQUUsQ0FBQztJQUM3QyxLQUFLLENBQUNDLEVBQUUsR0FBR0osTUFBTSxDQUFDSSxFQUFFO0lBQ3BCLEtBQUssQ0FBQ0EsRUFBRSxDQUFDQyxVQUFVLENBQUMsQ0FBWSxhQUFFQyxTQUFTLENBQUMsQ0FBQztRQUFDSCxRQUFRO0lBQUMsQ0FBQztBQUM1RCxDQUFDO2VBRWNJLE9BQU8sQ0FBQ0MsR0FBRyxFQUFFQyxHQUFHLEVBQUUsQ0FBQztJQUM5QixFQUFFLEVBQUVELEdBQUcsQ0FBQ0UsTUFBTSxLQUFLLENBQU0sT0FBRSxDQUFDO1FBQ3hCLEtBQUssQ0FBQ0MsU0FBUyxHQUFHSCxHQUFHLENBQUNJLElBQUksQ0FBQ0MsS0FBSztRQUVoQyxFQUFFLEdBQUdGLFNBQVMsS0FBS0EsU0FBUyxDQUFDRyxRQUFRLENBQUMsQ0FBRyxLQUFHLENBQUM7WUFDekNMLEdBQUcsQ0FBQ00sTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7Z0JBQUNDLE9BQU8sRUFBRSxDQUF3QjtZQUFDLENBQUM7WUFDMUQsTUFBTTtRQUNWLENBQUM7UUFDRCxHQUFHLENBQUNqQixNQUFNO1FBQ1YsR0FBRyxDQUFDLENBQUM7WUFDREEsTUFBTSxHQUFHLEtBQUssQ0FBQ0QsZUFBZTtRQUNsQyxDQUFDLENBQUMsS0FBSyxFQUFFbUIsS0FBSyxFQUFFLENBQUM7WUFDYlQsR0FBRyxDQUFDTSxNQUFNLENBQUMsR0FBRyxFQUFFQyxJQUFJLENBQUMsQ0FBQztnQkFBQ0MsT0FBTyxFQUFFLENBQW9DO1lBQUMsQ0FBQztZQUN0RSxNQUFNO1FBQ1YsQ0FBQztRQUVELEdBQUcsQ0FBQyxDQUFDO1lBQ0QsS0FBSyxDQUFDZixjQUFjLENBQUNGLE1BQU0sRUFBRSxDQUFDO2dCQUFDYSxLQUFLLEVBQUVGLFNBQVM7WUFBQyxDQUFDO1lBQ2pEWCxNQUFNLENBQUNtQixLQUFLO1FBQ2hCLENBQUMsQ0FBQyxLQUFLLEVBQUVELE1BQUssRUFBRSxDQUFDO1lBQ2JULEdBQUcsQ0FBQ00sTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7Z0JBQUNDLE9BQU8sRUFBRSxDQUF3QjtZQUFDLENBQUM7WUFDMUQsTUFBTTtRQUNWLENBQUM7UUFFRFIsR0FBRyxDQUFDTSxNQUFNLENBQUMsR0FBRyxFQUFFQyxJQUFJLENBQUMsQ0FBQztZQUFDQyxPQUFPLEVBQUUsQ0FBWTtRQUFDLENBQUM7SUFDbEQsQ0FBQztBQUNMLENBQUM7QUFFRCxpRUFBZVYsT0FBTyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcmVhY3QtY291cnNlMDcvLi9wYWdlcy9hcGkvbmV3c2xldHRlci5qcz8xMGFiIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vbmdvQ2xpZW50IH0gZnJvbSAnbW9uZ29kYic7XG5cbmFzeW5jIGZ1bmN0aW9uIGNvbm5lY3REYXRhYmFzZSgpIHtcbiAgICBjb25zdCBjbGllbnQgPSBhd2FpdCBNb25nb0NsaWVudC5jb25uZWN0KFxuICAgICAgICAnbW9uZ29kYitzcnY6Ly9kZXZlbG9wZXI6cmFoYXNpYUBjbHVzdGVyMC5tdjByNS5tb25nb2RiLm5ldC9ldmVudHM/cmV0cnlXcml0ZXM9dHJ1ZSZ3PW1ham9yaXR5J1xuICAgICk7XG4gICAgcmV0dXJuIGNsaWVudDtcbn1cblxuYXN5bmMgZnVuY3Rpb24gaW5zZXJ0RG9jdW1lbnQoY2xpZW50LCBkb2N1bWVudCkge1xuICAgIGNvbnN0IGRiID0gY2xpZW50LmRiKCk7XG4gICAgYXdhaXQgZGIuY29sbGVjdGlvbignbmV3c2xldHRlcicpLmluc2VydE9uZSh7IGRvY3VtZW50IH0pO1xufVxuXG5hc3luYyBmdW5jdGlvbiBoYW5kbGVyKHJlcSwgcmVzKSB7XG4gICAgaWYgKHJlcS5tZXRob2QgPT09ICdQT1NUJykge1xuICAgICAgICBjb25zdCB1c2VyRW1haWwgPSByZXEuYm9keS5lbWFpbDtcblxuICAgICAgICBpZiAoIXVzZXJFbWFpbCB8fCAhdXNlckVtYWlsLmluY2x1ZGVzKCdAJykpIHtcbiAgICAgICAgICAgIHJlcy5zdGF0dXMoNDIyKS5qc29uKHsgbWVzc2FnZTogJ0ludmFsaWQgZW1haWwgYWRkcmVzcy4nIH0pO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGxldCBjbGllbnRcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNsaWVudCA9IGF3YWl0IGNvbm5lY3REYXRhYmFzZSgpO1xuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgcmVzLnN0YXR1cyg1MDApLmpzb24oeyBtZXNzYWdlOiAnQ29ubmVjdGluZyB0byB0aGUgZGF0YWJhc2UgZmFpbGVkIScgfSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgaW5zZXJ0RG9jdW1lbnQoY2xpZW50LCB7IGVtYWlsOiB1c2VyRW1haWwgfSk7XG4gICAgICAgICAgICBjbGllbnQuY2xvc2UoKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKHsgbWVzc2FnZTogJ0luc2VydGluZyBkYXRhIGZhaWxlZCEnIH0pO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXMuc3RhdHVzKDIwMSkuanNvbih7IG1lc3NhZ2U6ICdTaWduZWQgdXAhJyB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGhhbmRsZXI7Il0sIm5hbWVzIjpbIk1vbmdvQ2xpZW50IiwiY29ubmVjdERhdGFiYXNlIiwiY2xpZW50IiwiY29ubmVjdCIsImluc2VydERvY3VtZW50IiwiZG9jdW1lbnQiLCJkYiIsImNvbGxlY3Rpb24iLCJpbnNlcnRPbmUiLCJoYW5kbGVyIiwicmVxIiwicmVzIiwibWV0aG9kIiwidXNlckVtYWlsIiwiYm9keSIsImVtYWlsIiwiaW5jbHVkZXMiLCJzdGF0dXMiLCJqc29uIiwibWVzc2FnZSIsImVycm9yIiwiY2xvc2UiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/api/newsletter.js\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/api/newsletter.js"));
module.exports = __webpack_exports__;

})();