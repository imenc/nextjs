"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/newsletter";
exports.ids = ["pages/api/newsletter"];
exports.modules = {

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("mongodb");

/***/ }),

/***/ "./helpers/db-util.js":
/*!****************************!*\
  !*** ./helpers/db-util.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"connectDatabase\": () => (/* binding */ connectDatabase),\n/* harmony export */   \"insertDocument\": () => (/* binding */ insertDocument),\n/* harmony export */   \"getAllDocuments\": () => (/* binding */ getAllDocuments)\n/* harmony export */ });\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongodb */ \"mongodb\");\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_0__);\n\nasync function connectDatabase() {\n    const client = await mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect('mongodb+srv://developer:rahasia@cluster0.mv0r5.mongodb.net/events?retryWrites=true&w=majority');\n    return client;\n}\nasync function insertDocument(client, collection, document) {\n    const db = client.db();\n    const result = await db.collection(collection).insertOne(document);\n    return result;\n}\nasync function getAllDocuments(client, collection, sort) {\n    const db = client.db();\n    const documents = await db.collection(collection).find().sort(sort).toArray();\n    return documents;\n}\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9oZWxwZXJzL2RiLXV0aWwuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBcUM7QUFFOUIsZUFBZUMsZUFBZSxHQUFHLENBQUM7SUFDckMsS0FBSyxDQUFDQyxNQUFNLEdBQUcsS0FBSyxDQUFDRix3REFBbUIsQ0FDcEMsQ0FBK0Y7SUFFbkcsTUFBTSxDQUFDRSxNQUFNO0FBQ2pCLENBQUM7QUFFTSxlQUFlRSxjQUFjLENBQUNGLE1BQU0sRUFBRUcsVUFBVSxFQUFDQyxRQUFRLEVBQUUsQ0FBQztJQUMvRCxLQUFLLENBQUNDLEVBQUUsR0FBR0wsTUFBTSxDQUFDSyxFQUFFO0lBQ3BCLEtBQUssQ0FBQ0MsTUFBTSxHQUFHLEtBQUssQ0FBQ0QsRUFBRSxDQUFDRixVQUFVLENBQUNBLFVBQVUsRUFBRUksU0FBUyxDQUFDSCxRQUFRO0lBQ2pFLE1BQU0sQ0FBQ0UsTUFBTTtBQUNqQixDQUFDO0FBRU0sZUFBZUUsZUFBZSxDQUFDUixNQUFNLEVBQUVHLFVBQVUsRUFBRU0sSUFBSSxFQUFFLENBQUM7SUFDN0QsS0FBSyxDQUFDSixFQUFFLEdBQUdMLE1BQU0sQ0FBQ0ssRUFBRTtJQUNwQixLQUFLLENBQUNLLFNBQVMsR0FBRyxLQUFLLENBQUNMLEVBQUUsQ0FDckJGLFVBQVUsQ0FBQ0EsVUFBVSxFQUNyQlEsSUFBSSxHQUNKRixJQUFJLENBQUNBLElBQUksRUFDVEcsT0FBTztJQUNaLE1BQU0sQ0FBQ0YsU0FBUztBQUNwQixDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcmVhY3QtY291cnNlMDgvLi9oZWxwZXJzL2RiLXV0aWwuanM/ZjU3NyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb25nb0NsaWVudCB9IGZyb20gJ21vbmdvZGInO1xuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gY29ubmVjdERhdGFiYXNlKCkge1xuICAgIGNvbnN0IGNsaWVudCA9IGF3YWl0IE1vbmdvQ2xpZW50LmNvbm5lY3QoXG4gICAgICAgICdtb25nb2RiK3NydjovL2RldmVsb3BlcjpyYWhhc2lhQGNsdXN0ZXIwLm12MHI1Lm1vbmdvZGIubmV0L2V2ZW50cz9yZXRyeVdyaXRlcz10cnVlJnc9bWFqb3JpdHknXG4gICAgKTtcbiAgICByZXR1cm4gY2xpZW50O1xufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gaW5zZXJ0RG9jdW1lbnQoY2xpZW50LCBjb2xsZWN0aW9uLGRvY3VtZW50KSB7XG4gICAgY29uc3QgZGIgPSBjbGllbnQuZGIoKTtcbiAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBkYi5jb2xsZWN0aW9uKGNvbGxlY3Rpb24pLmluc2VydE9uZShkb2N1bWVudCk7XG4gICAgcmV0dXJuIHJlc3VsdDtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldEFsbERvY3VtZW50cyhjbGllbnQsIGNvbGxlY3Rpb24sIHNvcnQpIHtcbiAgICBjb25zdCBkYiA9IGNsaWVudC5kYigpO1xuICAgIGNvbnN0IGRvY3VtZW50cyA9IGF3YWl0IGRiXG4gICAgICAgIC5jb2xsZWN0aW9uKGNvbGxlY3Rpb24pXG4gICAgICAgIC5maW5kKClcbiAgICAgICAgLnNvcnQoc29ydClcbiAgICAgICAgLnRvQXJyYXkoKTtcbiAgICByZXR1cm4gZG9jdW1lbnRzO1xufSJdLCJuYW1lcyI6WyJNb25nb0NsaWVudCIsImNvbm5lY3REYXRhYmFzZSIsImNsaWVudCIsImNvbm5lY3QiLCJpbnNlcnREb2N1bWVudCIsImNvbGxlY3Rpb24iLCJkb2N1bWVudCIsImRiIiwicmVzdWx0IiwiaW5zZXJ0T25lIiwiZ2V0QWxsRG9jdW1lbnRzIiwic29ydCIsImRvY3VtZW50cyIsImZpbmQiLCJ0b0FycmF5Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./helpers/db-util.js\n");

/***/ }),

/***/ "./pages/api/newsletter.js":
/*!*********************************!*\
  !*** ./pages/api/newsletter.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _helpers_db_util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../helpers/db-util */ \"./helpers/db-util.js\");\n\nasync function handler(req, res) {\n    if (req.method === 'POST') {\n        const userEmail = req.body.email;\n        if (!userEmail || !userEmail.includes('@')) {\n            res.status(422).json({\n                message: 'Invalid email address.'\n            });\n            return;\n        }\n        let client;\n        try {\n            client = await (0,_helpers_db_util__WEBPACK_IMPORTED_MODULE_0__.connectDatabase)();\n        } catch (error) {\n            res.status(500).json({\n                message: 'Connecting to the database failed!'\n            });\n            return;\n        }\n        try {\n            await (0,_helpers_db_util__WEBPACK_IMPORTED_MODULE_0__.insertDocument)(client, 'newsletter', {\n                email: userEmail\n            });\n            client.close();\n        } catch (error1) {\n            res.status(500).json({\n                message: 'Inserting data failed!'\n            });\n            return;\n        }\n        res.status(201).json({\n            message: 'Signed up!'\n        });\n    }\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9hcGkvbmV3c2xldHRlci5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7OztBQUF1RTtlQUd4REUsT0FBTyxDQUFDQyxHQUFHLEVBQUVDLEdBQUcsRUFBRSxDQUFDO0lBQzlCLEVBQUUsRUFBRUQsR0FBRyxDQUFDRSxNQUFNLEtBQUssQ0FBTSxPQUFFLENBQUM7UUFDeEIsS0FBSyxDQUFDQyxTQUFTLEdBQUdILEdBQUcsQ0FBQ0ksSUFBSSxDQUFDQyxLQUFLO1FBRWhDLEVBQUUsR0FBR0YsU0FBUyxLQUFLQSxTQUFTLENBQUNHLFFBQVEsQ0FBQyxDQUFHLEtBQUcsQ0FBQztZQUN6Q0wsR0FBRyxDQUFDTSxNQUFNLENBQUMsR0FBRyxFQUFFQyxJQUFJLENBQUMsQ0FBQztnQkFBQ0MsT0FBTyxFQUFFLENBQXdCO1lBQUMsQ0FBQztZQUMxRCxNQUFNO1FBQ1YsQ0FBQztRQUNELEdBQUcsQ0FBQ0MsTUFBTTtRQUNWLEdBQUcsQ0FBQyxDQUFDO1lBQ0RBLE1BQU0sR0FBRyxLQUFLLENBQUNiLGlFQUFlO1FBQ2xDLENBQUMsQ0FBQyxLQUFLLEVBQUVjLEtBQUssRUFBRSxDQUFDO1lBQ2JWLEdBQUcsQ0FBQ00sTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7Z0JBQUNDLE9BQU8sRUFBRSxDQUFvQztZQUFDLENBQUM7WUFDdEUsTUFBTTtRQUNWLENBQUM7UUFFRCxHQUFHLENBQUMsQ0FBQztZQUNELEtBQUssQ0FBQ1gsZ0VBQWMsQ0FBQ1ksTUFBTSxFQUFFLENBQVksYUFBRSxDQUFDO2dCQUFDTCxLQUFLLEVBQUVGLFNBQVM7WUFBQyxDQUFDO1lBQy9ETyxNQUFNLENBQUNFLEtBQUs7UUFDaEIsQ0FBQyxDQUFDLEtBQUssRUFBRUQsTUFBSyxFQUFFLENBQUM7WUFDYlYsR0FBRyxDQUFDTSxNQUFNLENBQUMsR0FBRyxFQUFFQyxJQUFJLENBQUMsQ0FBQztnQkFBQ0MsT0FBTyxFQUFFLENBQXdCO1lBQUMsQ0FBQztZQUMxRCxNQUFNO1FBQ1YsQ0FBQztRQUVEUixHQUFHLENBQUNNLE1BQU0sQ0FBQyxHQUFHLEVBQUVDLElBQUksQ0FBQyxDQUFDO1lBQUNDLE9BQU8sRUFBRSxDQUFZO1FBQUMsQ0FBQztJQUNsRCxDQUFDO0FBQ0wsQ0FBQztBQUVELGlFQUFlVixPQUFPLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9yZWFjdC1jb3Vyc2UwOC8uL3BhZ2VzL2FwaS9uZXdzbGV0dGVyLmpzPzEwYWIiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY29ubmVjdERhdGFiYXNlLCBpbnNlcnREb2N1bWVudCB9IGZyb20gJy4uLy4uL2hlbHBlcnMvZGItdXRpbCc7XG4gXG5cbmFzeW5jIGZ1bmN0aW9uIGhhbmRsZXIocmVxLCByZXMpIHtcbiAgICBpZiAocmVxLm1ldGhvZCA9PT0gJ1BPU1QnKSB7XG4gICAgICAgIGNvbnN0IHVzZXJFbWFpbCA9IHJlcS5ib2R5LmVtYWlsO1xuXG4gICAgICAgIGlmICghdXNlckVtYWlsIHx8ICF1c2VyRW1haWwuaW5jbHVkZXMoJ0AnKSkge1xuICAgICAgICAgICAgcmVzLnN0YXR1cyg0MjIpLmpzb24oeyBtZXNzYWdlOiAnSW52YWxpZCBlbWFpbCBhZGRyZXNzLicgfSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGNsaWVudFxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY2xpZW50ID0gYXdhaXQgY29ubmVjdERhdGFiYXNlKCk7XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICByZXMuc3RhdHVzKDUwMCkuanNvbih7IG1lc3NhZ2U6ICdDb25uZWN0aW5nIHRvIHRoZSBkYXRhYmFzZSBmYWlsZWQhJyB9KTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBhd2FpdCBpbnNlcnREb2N1bWVudChjbGllbnQsICduZXdzbGV0dGVyJywgeyBlbWFpbDogdXNlckVtYWlsIH0pO1xuICAgICAgICAgICAgY2xpZW50LmNsb3NlKCk7XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICByZXMuc3RhdHVzKDUwMCkuanNvbih7IG1lc3NhZ2U6ICdJbnNlcnRpbmcgZGF0YSBmYWlsZWQhJyB9KTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmVzLnN0YXR1cygyMDEpLmpzb24oeyBtZXNzYWdlOiAnU2lnbmVkIHVwIScgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBoYW5kbGVyO1xuIl0sIm5hbWVzIjpbImNvbm5lY3REYXRhYmFzZSIsImluc2VydERvY3VtZW50IiwiaGFuZGxlciIsInJlcSIsInJlcyIsIm1ldGhvZCIsInVzZXJFbWFpbCIsImJvZHkiLCJlbWFpbCIsImluY2x1ZGVzIiwic3RhdHVzIiwianNvbiIsIm1lc3NhZ2UiLCJjbGllbnQiLCJlcnJvciIsImNsb3NlIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/api/newsletter.js\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/api/newsletter.js"));
module.exports = __webpack_exports__;

})();