"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/contact";
exports.ids = ["pages/api/contact"];
exports.modules = {

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("mongodb");

/***/ }),

/***/ "./pages/api/contact.js":
/*!******************************!*\
  !*** ./pages/api/contact.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongodb */ \"mongodb\");\n/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_0__);\n\nasync function handler(req, res) {\n    if (req.method === 'POST') {\n        const { email , name , message  } = req.body;\n        if (!email || !email.includes('@') || !name || name.trim() === '' || !message || message.trim() === '') {\n            res.status(422).json({\n                message: 'Invalid input.'\n            });\n            return;\n        }\n        const newMessage = {\n            email,\n            name,\n            message\n        };\n        let client;\n        // 'mongodb+srv://developer:rahasia@cluster0.mv0r5.mongodb.net/my-site?retryWrites=true&w=majority'\n        const connectionString = 'mongodb+srv://${process.env.mongodb_username}:${process.env.mongodb_password}@${process.env.mongodb_clustername}.mv0r5.mongodb.net/${process.env.mongodb_database}?retryWrites=true&w=majority';\n        try {\n            client = await mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(connectionString);\n        } catch (error) {\n            console.log('ERROR : ', error);\n            res.status(500).json({\n                message: 'Could not connect to database.'\n            });\n            return;\n        }\n        const db = client.db();\n        try {\n            const result = await db.collection('messages').insertOne(newMessage);\n            newMessage.id = result.insertedId; // auto inserted id\n        } catch (error1) {\n            client.close();\n            res.status(500).json({\n                message: 'Storing message failed!'\n            });\n            return;\n        }\n        client.close();\n        res.status(201).json({\n            message: 'Successfully store message',\n            message: newMessage\n        });\n    }\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9hcGkvY29udGFjdC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7QUFBcUM7ZUFFdEJDLE9BQU8sQ0FBQ0MsR0FBRyxFQUFFQyxHQUFHLEVBQUUsQ0FBQztJQUNoQyxFQUFFLEVBQUVELEdBQUcsQ0FBQ0UsTUFBTSxLQUFLLENBQU0sT0FBRSxDQUFDO1FBQzFCLEtBQUssQ0FBQyxDQUFDLENBQUNDLEtBQUssR0FBRUMsSUFBSSxHQUFFQyxPQUFPLEVBQUMsQ0FBQyxHQUFHTCxHQUFHLENBQUNNLElBQUk7UUFDekMsRUFBRSxHQUFHSCxLQUFLLEtBQUtBLEtBQUssQ0FBQ0ksUUFBUSxDQUFDLENBQUcsUUFBTUgsSUFBSSxJQUFJQSxJQUFJLENBQUNJLElBQUksT0FBTyxDQUFFLE1BQUtILE9BQU8sSUFBSUEsT0FBTyxDQUFDRyxJQUFJLE9BQU8sQ0FBRSxHQUFFLENBQUM7WUFDdkdQLEdBQUcsQ0FBQ1EsTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7Z0JBQUNMLE9BQU8sRUFBRSxDQUFnQjtZQUFDLENBQUM7WUFDbEQsTUFBTTtRQUNSLENBQUM7UUFDRCxLQUFLLENBQUNNLFVBQVUsR0FBRyxDQUFDO1lBQUNSLEtBQUs7WUFBRUMsSUFBSTtZQUFFQyxPQUFPO1FBQUMsQ0FBQztRQUMzQyxHQUFHLENBQUNPLE1BQU07UUFDVixFQUFtRztRQUNuRyxLQUFLLENBQUNDLGdCQUFnQixHQUFHLENBQWdNO1FBQ3pOLEdBQUcsQ0FBQyxDQUFDO1lBQ0hELE1BQU0sR0FBRyxLQUFLLENBQUNkLHdEQUFtQixDQUFDZSxnQkFBZ0I7UUFDckQsQ0FBQyxDQUFDLEtBQUssRUFBRUUsS0FBSyxFQUFFLENBQUM7WUFDZkMsT0FBTyxDQUFDQyxHQUFHLENBQUMsQ0FBVSxXQUFFRixLQUFLO1lBQzdCZCxHQUFHLENBQUNRLE1BQU0sQ0FBQyxHQUFHLEVBQUVDLElBQUksQ0FBQyxDQUFDO2dCQUFDTCxPQUFPLEVBQUUsQ0FBZ0M7WUFBQyxDQUFDO1lBQ2xFLE1BQU07UUFDUixDQUFDO1FBQ0QsS0FBSyxDQUFDYSxFQUFFLEdBQUdOLE1BQU0sQ0FBQ00sRUFBRTtRQUNwQixHQUFHLENBQUMsQ0FBQztZQUNILEtBQUssQ0FBQ0MsTUFBTSxHQUFHLEtBQUssQ0FBQ0QsRUFBRSxDQUFDRSxVQUFVLENBQUMsQ0FBVSxXQUFFQyxTQUFTLENBQUNWLFVBQVU7WUFDbkVBLFVBQVUsQ0FBQ1csRUFBRSxHQUFHSCxNQUFNLENBQUNJLFVBQVUsQ0FBRyxDQUFtQjtRQUN6RCxDQUFDLENBQUMsS0FBSyxFQUFFUixNQUFLLEVBQUUsQ0FBQztZQUNmSCxNQUFNLENBQUNZLEtBQUs7WUFDWnZCLEdBQUcsQ0FBQ1EsTUFBTSxDQUFDLEdBQUcsRUFBRUMsSUFBSSxDQUFDLENBQUM7Z0JBQUNMLE9BQU8sRUFBRSxDQUF5QjtZQUFDLENBQUM7WUFDM0QsTUFBTTtRQUNSLENBQUM7UUFDRE8sTUFBTSxDQUFDWSxLQUFLO1FBQ1p2QixHQUFHLENBQUNRLE1BQU0sQ0FBQyxHQUFHLEVBQUVDLElBQUksQ0FDaEIsQ0FBQztZQUNHTCxPQUFPLEVBQUUsQ0FBNEI7WUFDckNBLE9BQU8sRUFBRU0sVUFBVTtRQUN2QixDQUFDO0lBRVAsQ0FBQztBQUNILENBQUM7QUFFRCxpRUFBZVosT0FBTyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcmVhY3QtY291cnNlMDkvLi9wYWdlcy9hcGkvY29udGFjdC5qcz8zNDkzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vbmdvQ2xpZW50IH0gZnJvbSAnbW9uZ29kYic7XG5cbmFzeW5jIGZ1bmN0aW9uIGhhbmRsZXIocmVxLCByZXMpIHtcbiAgaWYgKHJlcS5tZXRob2QgPT09ICdQT1NUJykge1xuICAgIGNvbnN0IHsgZW1haWwsIG5hbWUsIG1lc3NhZ2UgfSA9IHJlcS5ib2R5O1xuICAgIGlmICghZW1haWwgfHwgIWVtYWlsLmluY2x1ZGVzKCdAJykgfHwgIW5hbWUgfHwgbmFtZS50cmltKCkgPT09ICcnIHx8ICFtZXNzYWdlIHx8IG1lc3NhZ2UudHJpbSgpID09PSAnJykge1xuICAgICAgcmVzLnN0YXR1cyg0MjIpLmpzb24oeyBtZXNzYWdlOiAnSW52YWxpZCBpbnB1dC4nIH0pO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBjb25zdCBuZXdNZXNzYWdlID0geyBlbWFpbCwgbmFtZSwgbWVzc2FnZSB9O1xuICAgIGxldCBjbGllbnQ7XG4gICAgLy8gJ21vbmdvZGIrc3J2Oi8vZGV2ZWxvcGVyOnJhaGFzaWFAY2x1c3RlcjAubXYwcjUubW9uZ29kYi5uZXQvbXktc2l0ZT9yZXRyeVdyaXRlcz10cnVlJnc9bWFqb3JpdHknXG4gICAgY29uc3QgY29ubmVjdGlvblN0cmluZyA9ICdtb25nb2RiK3NydjovLyR7cHJvY2Vzcy5lbnYubW9uZ29kYl91c2VybmFtZX06JHtwcm9jZXNzLmVudi5tb25nb2RiX3Bhc3N3b3JkfUAke3Byb2Nlc3MuZW52Lm1vbmdvZGJfY2x1c3Rlcm5hbWV9Lm12MHI1Lm1vbmdvZGIubmV0LyR7cHJvY2Vzcy5lbnYubW9uZ29kYl9kYXRhYmFzZX0/cmV0cnlXcml0ZXM9dHJ1ZSZ3PW1ham9yaXR5JztcbiAgICB0cnkge1xuICAgICAgY2xpZW50ID0gYXdhaXQgTW9uZ29DbGllbnQuY29ubmVjdChjb25uZWN0aW9uU3RyaW5nKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5sb2coJ0VSUk9SIDogJywgZXJyb3IpO1xuICAgICAgcmVzLnN0YXR1cyg1MDApLmpzb24oeyBtZXNzYWdlOiAnQ291bGQgbm90IGNvbm5lY3QgdG8gZGF0YWJhc2UuJyB9KTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgY29uc3QgZGIgPSBjbGllbnQuZGIoKTtcbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgZGIuY29sbGVjdGlvbignbWVzc2FnZXMnKS5pbnNlcnRPbmUobmV3TWVzc2FnZSk7XG4gICAgICBuZXdNZXNzYWdlLmlkID0gcmVzdWx0Lmluc2VydGVkSWQ7ICAvLyBhdXRvIGluc2VydGVkIGlkXG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNsaWVudC5jbG9zZSgpO1xuICAgICAgcmVzLnN0YXR1cyg1MDApLmpzb24oeyBtZXNzYWdlOiAnU3RvcmluZyBtZXNzYWdlIGZhaWxlZCEnIH0pO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBjbGllbnQuY2xvc2UoKTtcbiAgICByZXMuc3RhdHVzKDIwMSkuanNvbihcbiAgICAgICAge1xuICAgICAgICAgICAgbWVzc2FnZTogJ1N1Y2Nlc3NmdWxseSBzdG9yZSBtZXNzYWdlJyxcbiAgICAgICAgICAgIG1lc3NhZ2U6IG5ld01lc3NhZ2VcbiAgICAgICAgfVxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgaGFuZGxlcjsiXSwibmFtZXMiOlsiTW9uZ29DbGllbnQiLCJoYW5kbGVyIiwicmVxIiwicmVzIiwibWV0aG9kIiwiZW1haWwiLCJuYW1lIiwibWVzc2FnZSIsImJvZHkiLCJpbmNsdWRlcyIsInRyaW0iLCJzdGF0dXMiLCJqc29uIiwibmV3TWVzc2FnZSIsImNsaWVudCIsImNvbm5lY3Rpb25TdHJpbmciLCJjb25uZWN0IiwiZXJyb3IiLCJjb25zb2xlIiwibG9nIiwiZGIiLCJyZXN1bHQiLCJjb2xsZWN0aW9uIiwiaW5zZXJ0T25lIiwiaWQiLCJpbnNlcnRlZElkIiwiY2xvc2UiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/api/contact.js\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/api/contact.js"));
module.exports = __webpack_exports__;

})();