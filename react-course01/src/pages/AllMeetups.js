import MeetupList from "../components/meetups/MeetupList";
import { useEffect, useState } from "react";

function AllMeetupsPage() {
    const[isLoading, setIsLoading] = useState(true);
    const[loadedMeetups, setLoadedMeetups] = useState([])

    // useEffect( () => {}, [] );
    useEffect( () => {
        setIsLoading(true);
        fetch(
            'https://react-course01-63fab-default-rtdb.asia-southeast1.firebasedatabase.app/meetups.json'
        ).then((response) => {
            return response.json();
        }).then((data) => {
            // Transform the data
            const meetups = [];
            for (const key in data) {
                const meetup = {
                    id: key,
                    ...data[key]
                };
                meetups.push(meetup);
            }
            setIsLoading(false);
            setLoadedMeetups(meetups);
        });
    }, [] );

    if (isLoading) {
        return(
            <section>
                <p>Loading...</p>
            </section>
        );
    }

    return(
        <section>
            <h1>All Meetups</h1>
            <MeetupList meetups={loadedMeetups} />
        </section>
    );
}

export default AllMeetupsPage;