"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/clients/[id]";
exports.ids = ["pages/clients/[id]"];
exports.modules = {

/***/ "./pages/clients/[id]/index.js":
/*!*************************************!*\
  !*** ./pages/clients/[id]/index.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ \"react/jsx-runtime\");\n/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);\n\nfunction CLientProjectsPage() {\n    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"div\", {\n        __source: {\n            fileName: \"/Users/bsi_2019_05/udemy/nextjs/react-course02/pages/clients/[id]/index.js\",\n            lineNumber: 3,\n            columnNumber: 9\n        },\n        __self: this,\n        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(\"h1\", {\n            __source: {\n                fileName: \"/Users/bsi_2019_05/udemy/nextjs/react-course02/pages/clients/[id]/index.js\",\n                lineNumber: 4,\n                columnNumber: 13\n            },\n            __self: this,\n            children: \"The Projects of a Given Client\"\n        })\n    }));\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CLientProjectsPage);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9jbGllbnRzL1tpZF0vaW5kZXguanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7OztTQUFTQSxrQkFBa0IsR0FBRSxDQUFDO0lBQzFCLE1BQU0sc0VBQ0RDLENBQUc7Ozs7Ozs7dUZBQ0NDLENBQUU7Ozs7Ozs7c0JBQUMsQ0FBOEI7OztBQUc5QyxDQUFDO0FBRUQsaUVBQWVGLGtCQUFrQixFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcmVhY3QtY291cnNlMDIvLi9wYWdlcy9jbGllbnRzL1tpZF0vaW5kZXguanM/ZTE1NiJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBDTGllbnRQcm9qZWN0c1BhZ2UoKXtcbiAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGgxPlRoZSBQcm9qZWN0cyBvZiBhIEdpdmVuIENsaWVudDwvaDE+XG4gICAgICAgIDwvZGl2PlxuICAgICk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IENMaWVudFByb2plY3RzUGFnZTsiXSwibmFtZXMiOlsiQ0xpZW50UHJvamVjdHNQYWdlIiwiZGl2IiwiaDEiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/clients/[id]/index.js\n");

/***/ }),

/***/ "react/jsx-runtime":
/*!************************************!*\
  !*** external "react/jsx-runtime" ***!
  \************************************/
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/clients/[id]/index.js"));
module.exports = __webpack_exports__;

})();