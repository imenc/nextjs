import { useRouter } from 'next/router';

function SelectedClientsProjectPage(){
    const router = useRouter();
    console.log(router.query);
    return(
        <div>
            <h1>The Project Page for Specific Project for Selected Client</h1>
        </div>
    );
}

export default SelectedClientsProjectPage;